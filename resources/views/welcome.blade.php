<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <script src="//{{ Request::getHost() }}:6001/socket.io/socket.io.js"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

        <title>20/20</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                /*height: 100vh;*/
                margin: 0;
                background: #e6f4fc url('images/wallpaper.png') no-repeat fixed bottom;
                background-size: 100% auto;
                /*background-size: cover;*/
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .slidecontainer {
                width: 100%; /* Width of the outside container */
            }

            /* The slider itself */
            .slider {
                -webkit-appearance: none;  /* Override default CSS styles */
                appearance: none;
                width: 100%; /* Full-width */
                height: 100px; /* Specified height */
                background: #0db7ed; /* Grey background */
                outline: none; /* Remove outline */
                opacity: 0.7; /* Set transparency (for mouse-over effects on hover) */
                -webkit-transition: .2s; /* 0.2 seconds transition on hover */
                transition: opacity .2s;
            }

            /* Mouse-over effects */
            .slider:hover {
                opacity: 1; /* Fully shown on mouse-over */
            }

            /* The slider handle (use -webkit- (Chrome, Opera, Safari, Edge) and -moz- (Firefox) to override default look) */
            .slider::-webkit-slider-thumb {
                -webkit-appearance: none; /* Override default look */
                appearance: none;
                width: 100px; /* Set a specific slider handle width */
                height: 100px; /* Slider handle height */
                background: #384d54; /* Green background */
                cursor: pointer; /* Cursor on hover */
            }

            .slider::-moz-range-thumb {
                width: 100px; /* Set a specific slider handle width */
                height: 100px; /* Slider handle height */
                background: #384d54; /* Green background */
                cursor: pointer; /* Cursor on hover */
            }

            .bigtext {
                font-size: 144pt;
                color: #384d54;
            }

            .smalltext {
                font-size: 48pt;
                color: #384d54;
            }

            .bigbutton {
                background-color: transparent;
                border: none;
            }

            .dockerbuttonimg {
                width: 250px;
                height: auto;
            }

            .litany {
                width: 50px;
                height: auto;
            }

            .tinytext {
                font-size: 18pt;
                color: #384d54;
            }

            /*#slide {*/
                /*background: #384d54;*/
                /*-webkit-animation: slide 0.5s forwards;*/
                /*-webkit-animation-delay: 2s;*/
                /*animation: slide 0.5s forwards;*/
            /*}*/

            /*@-webkit-keyframes slide {*/
                /*100% { left: 0; }*/
            /*}*/

            /*@keyframes slide {*/
                /*100% { left: 0; }*/
            /*}*/
        </style>
    </head>
    <body>
    <div id="app">
        <div class="flex-center position-ref">
            <div class="container-fluid">
                <div class="x-100">

                    <div class="row">
                        <div class="col-12">
                            <div class="flex-center smalltext">
                                <div id="msgsuccess" style="display: inline-block;">
                                    Maak containers aan...
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-9">

                            <div class="row">
                                <div class="col-12">
                                    <div class="flex-center bigtext" id="textvalue">
                                        0
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-10 offset-1">
                                    <div class="slidecontainer">
                                        <input type="range" min="1" max="10" value="1" class="slider" id="slider">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="flex-center">
                                <a href="#" class="bigbutton" onclick="sendValue()"><img src="{{ asset('images/button.png') }}" class="dockerbuttonimg" /></a>
                                </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="flex-center tinytext">
                                        <div id="msgdetail" style="display: inline-block;">
                                            Antissa:~ Maximilian$
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-3 tinytext" id="messagebox">
                            Binnenkomende berichten<br />
                            van containers:<br />
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <!-- Idd een scroll bar een knop en een uitvoerveld,
     zorg dat wanneer op een knop wordt gedrukt de waarde van je
     scrollbar wordt doorgegeven aan het bash script.
     Zorg ook voor een listener en een outputveld, zodat
     andere containers naar die listener kunnen sturen.
     De container zelf moet je in principe niet maken
     maar mag wel als je goesting hebt :wink: -->


    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    {{--<script type="module" src="js/messagereceiver.js"></script>--}}
    <script type="text/javascript" src="/js/app.js"></script>

    <script>

        $(document).ready(function() {
            $('.alert').hide();
            $('#textvalue').html($('#slider').val());
        });

        $('#slider').on('input', function() {
            console.log("Value change");
            $('#textvalue').html($('#slider').val());
        });

        function sendValue() {
            $.ajax({
                type:'POST',
                url:'/api/runbash',
                dataType: 'JSON',
                data: {
                    "value": $('#slider').val(),
                },
                success: function(data) {
                    $("#msgsuccess").html(data.msgSuccess);
                    $("#msgdetail").html(data.msgDetail);
                },
                error: function (request, status, error) {
                    alert(request.responseText);
                }
            });
        }

    </script>

    {{--public function storeValue(Request $request, $value)--}}
    {{--{--}}
    {{--$request->session()->put('value', $value);--}}
    {{--}--}}

    </body>
</html>

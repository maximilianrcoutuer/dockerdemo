<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class AjaxController extends Controller {

    public function runbash(Request $request) {

        // extract slider value from json
        $value = '';
        if($request->ajax()) {
            $value = $request->value;
        }

        // run process
        $process = new Process(array('ls', '-la'));     // Vervang dit door je eigen code.
        $process->run();

        // command failed
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }
        // build return messages
        $msgSuccess = ''.$value.' containers aangemaakt!';
        for($i = 0; $i < $value; $i ++) {
            $msgSuccess.=' <img src="images/button.png" class="litany" />';
        }

        $msgDetail = $process->getOutput();

        // return
        return response()->json(array('msgSuccess'=> $msgSuccess, 'msgDetail' => $msgDetail), 200);
    }
}
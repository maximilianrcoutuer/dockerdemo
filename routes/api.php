<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/runbash','AjaxController@runbash');

Route::get('/helloworld', function() {
    broadcast(new \App\Events\MessageReceived('DAB ON EM'));
});

Route::get('/message/{string?}', function($string = null) {
    broadcast(new \App\Events\MessageReceived($string));
});


//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
